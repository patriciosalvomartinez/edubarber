<?php

require_once ('model/usuario.php');

class UsuarioController{
    private $modelo;

    public function __construct(){
       $this->modelo = new Usuario();
    }
    
    public function PageIndex(){
        require_once 'view/pages/index.php';
    }

    public function PageGaleria(){
        require_once 'view/pages/galeria.php';
    }

    public function PageAgendar(){
        if (isset($_SESSION['IsActive'])) {
            require_once 'view/pages/agendar.php';    
        }else{
            require_once 'view/pages/index.php'; 
            echo '
            <script>
            alert("debe estar logueado");
            </script>
            ';        
        }
        
    }

    public function PageContact(){
        require_once 'view/pages/contacto.php';
    }

    public function RegistrarUsuario()
    {
        $user = new Usuario();

        $user->nombre = $_POST['nombre'];
        $user->apellido = $_POST['apellido'];
        $user->correo = $_POST['correo'];
        $user->password = $_POST['password'];
        $user->telefono = $_POST['telefono'];
        $user->idPerfil = 1;
        $existe=$this->modelo->UserExist($_POST['correo']);
        if($existe==true){
            echo '
            <script>
            alert("el usuario ya existe");
            </script>
            ';
        }else{
            $this->modelo->Registrar($user);
            echo '
            <script>
            alert("registrado");
            </script>
            ';
            
        }
        require_once 'view/pages/index.php';
    }

    public function Login(){
        
        $usser =new Usuario();

        $usser->correo=$_POST['email'];
        $usser->password=$_POST['pwd'];

        $existe=$this->modelo->UserExist($_POST['email']);

        if ($existe==true) {
            $credentialsArevalid = $usser->Login($usser);
            if($credentialsArevalid==true){
                require_once 'view/pages/agendar.php';
            }
        }else{
            require_once 'view/pages/index.php';
            echo '
            <script>
            alert("Usuario no existe");
            </script>
            ';
        };
    
    }
    public function Logout(){
        $this->modelo->Logout();
        require_once 'view/pages/index.php';
    }
}
?>