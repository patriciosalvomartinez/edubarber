-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.18-log - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para edubarber
DROP DATABASE IF EXISTS `edubarber`;
CREATE DATABASE IF NOT EXISTS `edubarber` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `edubarber`;

-- Volcando estructura para tabla edubarber.hora
DROP TABLE IF EXISTS `hora`;
CREATE TABLE IF NOT EXISTS `hora` (
  `idHora` int(11) NOT NULL AUTO_INCREMENT,
  `descHora` varchar(45) NOT NULL,
  PRIMARY KEY (`idHora`),
  UNIQUE KEY `idHora_UNIQUE` (`idHora`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla edubarber.hora: ~24 rows (aproximadamente)
DELETE FROM `hora`;
/*!40000 ALTER TABLE `hora` DISABLE KEYS */;
INSERT INTO `hora` (`idHora`, `descHora`) VALUES
	(1, '01:00'),
	(2, '02:00'),
	(3, '03:00'),
	(4, '04:00'),
	(5, '05:00'),
	(6, '06:00'),
	(7, '07:00'),
	(8, '08:00'),
	(9, '09:00'),
	(10, '10:00'),
	(11, '11:00'),
	(12, '12:00'),
	(13, '13:00'),
	(14, '14:00'),
	(15, '15:00'),
	(16, '16:00'),
	(17, '17:00'),
	(18, '18:00'),
	(19, '19:00'),
	(20, '20:00'),
	(21, '21:00'),
	(22, '22:00'),
	(23, '23:00'),
	(24, '00:00');
/*!40000 ALTER TABLE `hora` ENABLE KEYS */;

-- Volcando estructura para tabla edubarber.perfil
DROP TABLE IF EXISTS `perfil`;
CREATE TABLE IF NOT EXISTS `perfil` (
  `idPerfil` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idPerfil`),
  UNIQUE KEY `idPerfil_UNIQUE` (`idPerfil`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla edubarber.perfil: ~1 rows (aproximadamente)
DELETE FROM `perfil`;
/*!40000 ALTER TABLE `perfil` DISABLE KEYS */;
INSERT INTO `perfil` (`idPerfil`, `nombre`) VALUES
	(1, 'Administrador');
/*!40000 ALTER TABLE `perfil` ENABLE KEYS */;

-- Volcando estructura para tabla edubarber.reserva
DROP TABLE IF EXISTS `reserva`;
CREATE TABLE IF NOT EXISTS `reserva` (
  `idReserva` int(11) NOT NULL AUTO_INCREMENT,
  `fechaReserva` datetime DEFAULT NULL,
  `idUsuario` int(11) NOT NULL,
  `idHora` int(11) NOT NULL,
  PRIMARY KEY (`idReserva`),
  UNIQUE KEY `idReserva_UNIQUE` (`idReserva`),
  KEY `fk_reserva_usuario_idx` (`idUsuario`),
  KEY `fk_reserva_hora1_idx` (`idHora`),
  CONSTRAINT `fk_reserva_hora1` FOREIGN KEY (`idHora`) REFERENCES `hora` (`idHora`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reserva_usuario` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla edubarber.reserva: ~7 rows (aproximadamente)
DELETE FROM `reserva`;
/*!40000 ALTER TABLE `reserva` DISABLE KEYS */;
INSERT INTO `reserva` (`idReserva`, `fechaReserva`, `idUsuario`, `idHora`) VALUES
	(1, '2020-12-12 00:00:00', 1, 14),
	(2, '2020-12-12 00:00:00', 1, 15),
	(3, '2020-12-12 00:00:00', 5, 16),
	(4, '2020-12-13 00:00:00', 5, 14),
	(5, '2020-12-13 00:00:00', 5, 15),
	(6, '2020-12-13 00:00:00', 5, 16),
	(10, '2020-08-23 00:00:00', 5, 16);
/*!40000 ALTER TABLE `reserva` ENABLE KEYS */;

-- Volcando estructura para tabla edubarber.usuario
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `idPerfil` int(11) NOT NULL,
  PRIMARY KEY (`idUsuario`),
  UNIQUE KEY `idUsuario_UNIQUE` (`idUsuario`),
  KEY `fk_usuario_Perfil1_idx` (`idPerfil`),
  CONSTRAINT `fk_usuario_Perfil1` FOREIGN KEY (`idPerfil`) REFERENCES `perfil` (`idPerfil`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla edubarber.usuario: ~8 rows (aproximadamente)
DELETE FROM `usuario`;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`idUsuario`, `nombre`, `apellido`, `correo`, `password`, `telefono`, `idPerfil`) VALUES
	(1, 'Patricio', 'Salvo', 'patricio.salvo@vrclass.cl', '123456', '+56957714353', 1),
	(3, 'aaaaaaaaaaaaaaaaaaaaa', 'sass', 'p@gmail.com', 'hola', '646464646464', 1),
	(4, 'Ignacio', 'López', 'ignacho@gmail.com', '123456', '55555555555555', 1),
	(5, 'Ignacio López', 'López', 'sassasp@gmail.com', 'hola', '55555555555555', 1),
	(6, 'ddddddddddddddd', 'ddddddddddddd', 'g@gmail.com', '123456', '77777777777777777777', 1),
	(7, 'test desarrollo 2', 'sass', 'xxx@gmail.com', '123456', '55555555555555555', 1),
	(8, '', '', '', '', '', 1),
	(9, '', '', '', '', '', 1);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
