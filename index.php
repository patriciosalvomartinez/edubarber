<?php
session_start();
if (!isset($_REQUEST['c']))
{
   require_once 'controller/controller.usuario.php';
   $controller = 'UsuarioController';
   $controller = new $controller;
   $controller->PageIndex(); 
}else{
   $controller = strtolower($_REQUEST['c']);
   $accion = $_REQUEST['a'];
   require_once "controller/controller.$controller.php";
   $controller = ucwords($controller).'Controller';
   $controller = new $controller;
   call_user_func(array($controller,$accion));
}

?>