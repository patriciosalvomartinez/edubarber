<?php
    class Usuario{
        private $coon;
        public $idUsuario;
        public $idPerfil;
        public $nombre;
        public $correo;
        public $password;
        public $telefono;
        
        public function __construct(){
            try{
                require_once ('model/db.php');
                $this->coon = BaseDeDatos::Conexion();

            }catch(\Exception $e){
                die($e->getMessage());
            }
        }
        
        public function Registrar($user)
        {
            try {
            $strsql =  $this->coon ->prepare("INSERT INTO usuario(nombre,apellido,correo,password,telefono,idPerfil) values (?,?,?,?,?,?)");
            $strsql->execute(array($user->nombre, $user->apellido, $user->correo, $user->password, $user->telefono,$user->idPerfil));
            
            $coon=null;
            } catch (Exception $e){
                    die($e->getMessage());
            }
        }   

        public function Login($user){
            $sql='SELECT * FROM usuario WHERE correo= ? and password = ?';
            $strsql = $this->coon ->prepare($sql,array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $strsql->execute(array($user->correo, $user->password));
            $resultado = $strsql->fetchObject();
            $retorno=false;


            while($row = $resultado)
            if (!$row->nombre==null) {
                
                $_SESSION['idUsuario']=$row->idUsuario;
                $_SESSION['idPerfil']=$row->idPerfil;
                $_SESSION['nombre']=$row->nombre;
                $_SESSION['apellido']=$row->apellido;
                $_SESSION['password']=$row->password;
                $_SESSION['email']=$row->correo;
                $_SESSION['telefono']=$row->telefono;
                $_SESSION['IsActive']=1;
                
                $retorno=true;
                return $retorno;
            }else{
                
                $retorno=false;
                return $retorno;
            }
        }
        public function UserExist($email){
            try{
                $sql = $this->coon->prepare("SELECT * FROM usuario WHERE correo = ?");
                $sql->execute(array($email));
                $resultado = $sql->fetch(PDO::FETCH_OBJ);
                while ($row=$resultado) {
                    if (!$row->correo==null) {
                        $retorno=true;
                        return $retorno;
                    }else {
                        $retorno=false;
                        return $retorno;
                    }
                }
            }catch(Exception $ex){
                die($ex->getMessage());
            }
        }
        
        public function Logout(){
            session_unset();
            session_destroy();
        }
        

    }
?>