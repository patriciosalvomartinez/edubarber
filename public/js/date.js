$(document).ready(function() {
    $('#datepicker').datepicker({
        language: "es",
        todayBtn: "linked",
        format: "dd/mm/yyyy",
        todayHighlight: true,
        startDate: new Date()
    });    

    $( ".form-check" ).click(function() {
        total=0;
        //var favorite = [];
        $.each($("input[name='servicio']:checked"), function(){
            //favorite.push($(this).val());
            total=total+parseInt($(this).val());
        });
//        var servicio1=parseInt(document.getElementById('checkCorte').value)
        document.getElementById("subTotalInput").value = total;
      });
});