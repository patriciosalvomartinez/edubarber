        <!-- Masthead-->
        <header class="masthead">
            <div class="container h-100">
                <div class="row h-100 align-items-center justify-content-center text-center">
                    <div class="col-lg-10 align-self-end">
                        <h1 class="text-uppercase text-white font-weight-bold">EDUBARBER</h1>
                        <hr class="divider my-4" />
                    </div>
                    <div class="col-lg-8 align-self-baseline">
                        <p class="text-white-75 font-weight-light mb-5">Descubre una variedad de looks y encuentra el tuyo</p>
                        <a id="btnReserva" class="btn btn-primary btn-xl js-scroll-trigger" href="index.php?c=Usuario&a=PageAgendar">Reservar Hora</a>
                        
                    </div>
                </div>
            </div>
        </header>
