<!-- Gallery  -->
<div id="portfolio">
            <div class="container-fluid p-0">
                <div class="row no-gutters">
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="public/img/portfolio/fullsize/IMG_3899.JPG">
                            <img class="img-fluid" src="public/img/portfolio/thumbnails/IMG_3899.jpg" alt="" />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="public/img/portfolio/fullsize/IMG_5555.JPG">
                            <img class="img-fluid" src="public/img/portfolio/thumbnails/IMG_5555.jpg" alt="" />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="public/img/portfolio/fullsize/IMG_4033.jpg">
                            <img class="img-fluid" src="public/img/portfolio/thumbnails/IMG_4033.jpg" alt="" />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="public/img/portfolio/fullsize/IMG_3904.jpg">
                            <img class="img-fluid" src="public/img/portfolio/thumbnails/IMG_3904.jpg" alt="" />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="public/img/portfolio/fullsize/IMG_4006.jpg">
                            <img class="img-fluid" src="public/img/portfolio/thumbnails/IMG_4006.jpg" alt="" />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="public/img/portfolio/fullsize/IMG_666.JPG">
                            <img class="img-fluid" src="public/img/portfolio/thumbnails/IMG_666.jpg" alt="" />
                            <div class="portfolio-box-caption p-3">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
         <!-- End Gallery -->