         <!-- About me -->
         <section class="page-section bg-light" id="about">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">CONOCE MÁS ACERCA DE MÍ</h2>
                </div>
                <div class="row">
                  
                    <div class="col-lg-4">
                        <div class="team-member"><br>
                            <img class="mx-auto rounded-circle" src="public/img/team/eduardo.png" alt="" />
                            <h4><br>Eduardo López</h4>
                            <p class="text-muted">Tremendo traga sables</p>
                            <a class="btn btn-dark btn-social mx-2" href="https://api.whatsapp.com/send?phone=+56974064598" target="_blank"><i class="fab fa-whatsapp"></i></a>
                            <a class="btn btn-dark btn-social mx-2" href="https://www.facebook.com/eduthebarber/" target="_blank"><i class="fab fa-facebook-f"></i></a>
                            <a class="btn btn-dark btn-social mx-2" href="https://www.instagram.com/_edubarber/" target="_blank"><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
        </section>