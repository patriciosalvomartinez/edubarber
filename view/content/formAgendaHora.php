<div class="container">
    <div class="row formAgendaHora">
        <div class="col-10">
          <center><h2>AGENDA HORA CON EDU BARBER</h2></center>
            <div class="container">
                <div class="row">
                    <form>
                    <div class="input-group">
                        <input type="text" class="form-control-plaintext" id="nameUser" style="color:white;" value="<?php echo $_SESSION['nombre'];?>" readonly>
                        <input type="text" class="form-control-plaintext" id="lastNameUser" style="color:white;" value="<?php echo $_SESSION['apellido'];?>" readonly>
                    </div>
                        <fieldset class="form-group">
                            <div class="row">
                                <legend class="col-form-label col-sm-2 pt-0">Servicios:</legend>
                                <div class="col-sm-10">
                                  <div class="form-check">
                                      <input class="form-check-input" type="checkbox" id="checkCorte" name="servicio" value="6000">
                                      <label class="form-check-label" for="gridCheck1">
                                          Corte
                                      </label>
                                  </div>
                                  <div class="form-check">
                                      <input class="form-check-input" type="checkbox" id="checkDisenio" name="servicio" value="2000">
                                      <label class="form-check-label" for="gridCheck1">
                                          Diseño
                                      </label>
                                  </div>
                                  <div class="form-check">
                                      <input class="form-check-input" type="checkbox" id="checkBarba" name="servicio" value="1500">
                                      <label class="form-check-label" for="gridCheck1">
                                          Barba
                                      </label>
                                  </div>
                                  <div class="form-check">
                                      <input class="form-check-input" type="checkbox" id="checkTintura" name="servicio" value="40000">
                                      <label class="form-check-label" for="gridCheck1">
                                          Tintura
                                      </label>
                                  </div>
                                  
                                </div>
                            </div>

                            <div class="input-group mb-3">
                            
                            <div class="input-group-prepend">
                              <span class="input-group-text">$</span>
                            </div>
                            <input type="text" id="subTotalInput" class="form-control" value="" style="color: black;" readonly>
                          </div>
                        </fieldset>

                    </form>

                </div>
            </div>


        </div>
        <div class="col-4">

        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-4">        
            <div >
                <input id="datepicker" data-provide="datepicker-inline" class="datepicker" data-date-format="dd/mm/yyyy">
            </div>
        </div>
        <div class="col-4">
            <button type="button" class="btn btn-primary btn-lg">Large button</button>            
            <button type="button" class="btn btn-primary btn-lg">Large button</button>
            <button type="button" class="btn btn-primary btn-lg">Large button</button>
            <button type="button" class="btn btn-primary btn-lg">Large button</button>
            <button type="button" class="btn btn-primary btn-lg">Large button</button>
            <button type="button" class="btn btn-primary btn-lg">Large button</button>
            <button type="button" class="btn btn-primary btn-lg">Large button</button>
            <button type="button" class="btn btn-primary btn-lg">Large button</button>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-8">
            <div class="form-group">
                <textarea class="form-control" id="exampleFormControlTextarea1" placeholder="Ingrese mensaje adicional" rows="3"></textarea>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-6">
            <button type="button" class="btn btn-success btn-lg btn-block">Agendar</button>
        </div>
    </div>
</div>