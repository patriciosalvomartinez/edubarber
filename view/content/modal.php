        <!-- First Modal content-->

        <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

      <div class="modal-content">
        <div class="modal-header">
        <h2 for="header">Iniciar sesión</h2>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form id="login_Form" name="login_Form" class="from-horizontal" role="form" method="POST" action="index.php?c=Usuario&a=Login">

          <div class="form-group">
            <label for="emaillbl">Email</label>
            <input type="text" class="form-control" id="email" name="email">
          </div>

          <div class="form-group">
            <label for="pwdlbl">Password</label>
            <input type="password" class="form-control" id="pwd" name="pwd" >
          </div>

            <button type="submit" class="btn btn-default btn-success btn-block"><span class="glyphicon glyphicon-off"></span> Login</button>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      </div>
      </div>

      <!-- 2 MODAL REGISTER CONTENT -->
      
      <div class="modal fade" id="modalRegister" role="dialog">
    <div class="modal-dialog">
             
              <div class="modal-content">
              
        <div class="modal-header">
        <h2 for="header">REGISTRO</h2>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form role="form" method="POST" action="index.php?c=Usuario&a=RegistrarUsuario">
            
          <div class="form-group">
            <label for="usr">Nombre</label>
            <input type="text" class="form-control" name="nombre" id="nombre">
          </div>

          <div class="form-group">
            <label for="pwd">Apellido</label>
            <input type="text" class="form-control" name="apellido" id="apellido">
          </div>

          <div class="form-group">
            <label for="pwd">Email</label>
            <input type="text" class="form-control" name="correo" id="correo">
          </div>

          <div class="form-group">
            <label for="pwd">Password</label>
            <input type="password" class="form-control" name="password" id="password">
          </div>

          <div class="form-group">
            <label for="pwd">Télefono</label>
            <input type="text" class="form-control" name="telefono" id="telefono">
          </div>

            <button type="submit" class="btn btn-default btn-success btn-block"><span class="glyphicon glyphicon-off"></span>Registrarse</button>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
         </div>
      </div>
      
      