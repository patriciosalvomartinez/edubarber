<!DOCTYPE html>
<html class="wide wow-animation desktop landscape rd-navbar-fixed-linked" lang="en">
    <head>
        <?php require_once("view/partials/head.php"); ?>
    </head>
    <body>
        <?php require_once("view/partials/header.php"); ?>
            <?php require_once("view/content/formAgendaHora.php"); ?>
            <?php require_once("view/content/modal.php")?>
      <?php require_once("view/partials/footer.php") ?>
    </body>
</html>