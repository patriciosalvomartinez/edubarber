<!DOCTYPE html>
<html class="wide wow-animation desktop landscape rd-navbar-fixed-linked" lang="en">
    <head>
        <?php require_once("view/partials/head.php"); ?>
    </head>
    <body>
        <?php require_once("view/partials/header.php"); ?>
        
            <?php require_once("view/content/data1.php"); ?>
            <?php require_once("view/content/data2.php"); ?>
            <?php require_once("view/content/data3.php"); ?>
            <?php require_once("view/content/data4.php"); ?>
            <?php require_once("view/content/data5.php"); ?>
            <?php require_once("view/content/modal.php")?>
      <?php require_once("view/partials/footer.php") ?>
    </body>
</html>