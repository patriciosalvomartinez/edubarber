        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
            <div class="container">


                <!--RD Navbar Brand-->
                <div class="navbar-brand">
                    <a href="index.php">
                        <img src="public/img/icono/white2.png"  alt="Edubarber">
                    </a>
                </div>             
                <!--END RD Navbar Brand-->
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto my-2 my-lg-0">
                        <?php if (isset($_SESSION['IsActive'])){ ?>
                            <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php?c=Usuario&a=Logout" >CERRAR SESIÓN</a></li>                                       
                            <li class="nav-item"><a class="nav-link js-scroll-trigger" >BIENVENIDO <?php echo $_SESSION['nombre']; ?></a></li>               
                        <?php } ?>
                        
                        <?php if (!isset($_SESSION['IsActive'])){ ?>
                            <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#services">SERVICIOS</a></li>
                            <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#portfolio">GALERÍA</a></li>
                            <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#about">ACERCA DE MÍ</a></li>
                            <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#contact">CONTACTO</a></li>                            
                            <li class="nav-item"><a class="nav-link js-scroll-trigger" href="" data-toggle="modal" data-target="#myModal">INICIAR SESIÓN</a></li>               
                            <li class="nav-item"><a class="nav-link js-scroll-trigger" href="" data-toggle="modal" data-target="#modalRegister">REGISTRARSE</a></li>                                  
                        <?php } ?>

                    </ul>
                </div>
            </div>
        </nav>